
function next() {
  var f = document.forms[0];
  var pi = parseInt(f.pageId.value);
  if (pi < f.pageNum.value) {
    f.pageId.value = pi+1;
    f.submit();
  }
}

function prev() {
  var f = document.forms[0];
  var pi = parseInt(f.pageId.value);
  if (pi > 1) {
    f.pageId.value = pi-1;
    f.submit();
  }
}

function _submitPaged(pageid) {
  document.forms[0].pageId.value=pageid;
  document.forms[0].submit();
}

function confirmDelete() {
    return window.confirm('确实要删除该条记录吗？');
}

