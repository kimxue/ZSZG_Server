package com.begamer.card.json.command2;

import java.util.List;

import com.begamer.card.json.ErrorJson;
import com.begamer.card.json.PackElement;

public class LotResultJson extends ErrorJson
{
	public int n;//==水晶抽卡点==//
	public int d;//==屌丝券==//
	public int f;//==友情值==//
	public int c;//==水晶值==//
	public int t;//免费抽卡倒计时(秒)
	public List<PackElement> list;
		
	public int getD()
	{
		return d;
	}

	public void setD(int d)
	{
		this.d = d;
	}

	public int getF()
	{
		return f;
	}

	public void setF(int f)
	{
		this.f = f;
	}

	public List<PackElement> getList()
	{
		return list;
	}

	public void setList(List<PackElement> list)
	{
		this.list = list;
	}

	public int getN()
	{
		return n;
	}

	public void setN(int n)
	{
		this.n = n;
	}

	public int getC()
	{
		return c;
	}

	public void setC(int c)
	{
		this.c = c;
	}

	public int getT() {
		return t;
	}

	public void setT(int t) {
		this.t = t;
	}

}
