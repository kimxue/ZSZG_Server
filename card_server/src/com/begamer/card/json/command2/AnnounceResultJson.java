package com.begamer.card.json.command2;

import java.util.List;

import com.begamer.card.json.ErrorJson;
import com.begamer.card.model.pojo.Announce;

public class AnnounceResultJson extends ErrorJson{

	public List<Announce> announces;

	public List<Announce> getAnnounces()
	{
		return announces;
	}

	public void setAnnounces(List<Announce> announces)
	{
		this.announces = announces;
	}
	
	
}
