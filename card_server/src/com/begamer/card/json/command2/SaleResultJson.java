package com.begamer.card.json.command2;

import com.begamer.card.json.ErrorJson;

public class SaleResultJson extends ErrorJson
{
	//增加的金币
	public int addG;
	//playerGold
	public int g;	
	public int getAddG()
	{
		return addG;
	}
	public void setAddG(int addG)
	{
		this.addG = addG;
	}
	public int getG()
	{
		return g;
	}
	public void setG(int g)
	{
		this.g = g;
	}
}
