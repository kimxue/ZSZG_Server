package com.begamer.card.json.command2;

import com.begamer.card.json.BasicJson;

public class MazeBattleJson extends BasicJson {
	/**迷宫编号**/
	public int td;
	/**1普通 2boss 3精英**/
	public int t;
	/**迷宫位置**/
	public int state;
	
	public int getTd() {
		return td;
	}

	public void setTd(int td) {
		this.td = td;
	}

	public int getT() {
		return t;
	}

	public void setT(int t) {
		this.t = t;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}
	
}
