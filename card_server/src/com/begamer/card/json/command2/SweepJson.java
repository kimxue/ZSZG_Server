package com.begamer.card.json.command2;

import com.begamer.card.json.BasicJson;

public class SweepJson extends BasicJson
{
	public int sweepNum;//扫荡次数
	public int bNum;//场次
	public int md;//关卡id
	public int getSweepNum() {
		return sweepNum;
	}
	public void setSweepNum(int sweepNum) {
		this.sweepNum = sweepNum;
	}
	public int getbNum() {
		return bNum;
	}
	public void setbNum(int bNum) {
		this.bNum = bNum;
	}
	public int getMd() {
		return md;
	}
	public void setMd(int md) {
		this.md = md;
	}
	
	
}
