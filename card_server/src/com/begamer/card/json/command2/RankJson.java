package com.begamer.card.json.command2;

import com.begamer.card.json.BasicJson;

public class RankJson extends BasicJson
{
	public int type;//0,排位赛 1,天位赛 2,夺宝奇兵

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
	
}
