package com.begamer.card.json.command2;

import java.util.List;

import com.begamer.card.json.BasicJson;

public class EventBattleLogJson extends BasicJson {
	public List<String> bs;
	public int r;// result:1胜利,2失败//
	public int bNum;// 战斗场次
	// 死亡洞窟专有
	public int bv;// boss掉血
	public int t;// 1.死亡洞穴
	
	public List<String> getBs()
	{
		return bs;
	}
	
	public void setBs(List<String> bs)
	{
		this.bs = bs;
	}
	
	public int getR()
	{
		return r;
	}
	
	public void setR(int r)
	{
		this.r = r;
	}
	
	public int getBNum()
	{
		return bNum;
	}
	
	public void setBNum(int num)
	{
		bNum = num;
	}

	public int getBv()
	{
		return bv;
	}

	public void setBv(int bv)
	{
		this.bv = bv;
	}

	public int getT()
	{
		return t;
	}

	public void setT(int t)
	{
		this.t = t;
	}
	
}
