package com.begamer.card.json.command2;

import com.begamer.card.json.ErrorJson;

public class WishResultJson extends ErrorJson
{
	public String mazeWish;//已经许愿的物品 迷宫id-碎片id&迷宫id-碎片id
	public String mazeBossDrop;//可以许愿的物品  迷宫id-碎片id&迷宫id-碎片id
	public String getMazeWish() {
		return mazeWish;
	}
	public void setMazeWish(String mazeWish) {
		this.mazeWish = mazeWish;
	}
	public String getMazeBossDrop() {
		return mazeBossDrop;
	}
	public void setMazeBossDrop(String mazeBossDrop) {
		this.mazeBossDrop = mazeBossDrop;
	}
	
	
}
