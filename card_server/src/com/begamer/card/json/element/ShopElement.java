package com.begamer.card.json.element;

public class ShopElement
{
	public int id;//shop blackmarket 中的id        //登陆奖励id//           转盘档位id//           转动n次对应的id
	public int num;//已经购买的次数                   //登陆奖励是否领取标识    0未领取    1领取//      转盘档位id对应的转动次数//   转动n次对应的标识。是否领取，已经领取1，未领取0
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}
