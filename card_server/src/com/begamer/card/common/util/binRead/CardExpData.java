package com.begamer.card.common.util.binRead;

import java.util.HashMap;

import com.begamer.card.common.util.StringUtil;

public class CardExpData implements PropertyReader
{

	public int level;
	public int[] starexps;
	public int cost;
	
	private static HashMap<Integer, CardExpData> data=new HashMap<Integer, CardExpData>();
	
	@Override
	public void addData()
	{
		data.put(level, this);
	}

	@Override
	public void resetData()
	{
		data.clear();
	}

	@Override
	public void parse(String[] ss)
	{
		level=StringUtil.getInt(ss[0]);
		starexps=new int[ss.length-2];
		for(int i=0;i<starexps.length;i++)
		{
			starexps[i]=StringUtil.getInt(ss[1+i]);
		}
		cost =StringUtil.getInt(ss[ss.length-1]);
		addData();
	}

	public static CardExpData getData(int level)
	{
		return data.get(level);	
	}
	
}
