package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.begamer.card.common.util.StringUtil;
import com.begamer.card.log.ErrorLogger;

public class UseableItemData implements PropertyReader
{
	public int id;
	public String name;
	public int cost;
	public int uselevel;
	public int viprequest;
	public List<String> rewardInfo;
	
	private static HashMap<Integer, UseableItemData> data =new HashMap<Integer, UseableItemData>();
	private static final Logger errorlogger=ErrorLogger.logger;
	@Override
	public void addData()
	{
		data.put(id, this);
	}

	@Override
	public void parse(String[] ss)
	{
		int location=0;
		id =StringUtil.getInt(ss[location]);
		name =StringUtil.getString(ss[location+1]);
		cost =StringUtil.getInt(ss[location+2]);
		uselevel =StringUtil.getInt(ss[location+3]);
		viprequest =StringUtil.getInt(ss[location+4]);
		rewardInfo =new ArrayList<String>();
		for(int i=0;i<8;i++)
		{
			location =5+2*i;
			int rewardType =StringUtil.getInt(ss[location]);
			
			if(rewardType==0)
			{
				continue;
			}
			else
			{
				String reward =StringUtil.getString(ss[location+1]);
				try
				{
					if(rewardType<=5)
					{
						String []temp =reward.split(",");
						int goodsId =StringUtil.getInt(temp[0]);
						int num =StringUtil.getInt(temp[1]);
					}
				}
				catch (Exception e) 
				{
					errorlogger.error("加载useableitem表奖励物品格式错误:"+id,e);
					System.exit(0);
				}
				
				String info =rewardType+"-"+reward;
				rewardInfo.add(info);
			}
		}
		addData();
	}

	@Override
	public void resetData()
	{
		data.clear();
	}
	
	public static UseableItemData getUseableItemData(int index)
	{
		return data.get(index);
	}

}
