package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.List;

public class TaskData implements PropertyReader
{
	public int taskId;					
	public int map_number;
	public int ci_begin;
	public int ci_Id;
	public String ci_model;
	public float yPos;
	public float zoom;
	public String acceptWord;
	
	private static List<Integer> data=new ArrayList<Integer>();
	
	@Override
	public void addData()
	{
		if(!data.contains(map_number))
		{
			data.add(map_number);
		}
	}

	@Override
	public void parse(String[] ss)
	{
	}

	@Override
	public void resetData()
	{
		data.clear();
	}

	public static boolean haveTask(int missionId)
	{
		return data.contains(missionId);
	}
	
}
