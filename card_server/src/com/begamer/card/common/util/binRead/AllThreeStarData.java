package com.begamer.card.common.util.binRead;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.begamer.card.common.util.StringUtil;
import com.begamer.card.log.ErrorLogger;

public class AllThreeStarData implements PropertyReader
{
	//==编号==//	
	public int id;
	//==关卡类型==//
	public int type;
	//==地图==//
	public int map;
	//==区域==//
	public int zone;
	//==区域描述==//
	public String description;
	//==奖励类别==//
	public int rewardtype;
	//==奖励==//
	public String reward;
	
	private static HashMap<String, AllThreeStarData> data=new HashMap<String, AllThreeStarData>();
	private static final Logger errorlogger=ErrorLogger.logger;
	
	@Override
	public void addData()
	{
		try
		{
			if(rewardtype<=5)
			{
				String [] temp =reward.split(",");
				int rewardId =StringUtil.getInt(temp[0]);
				int num =StringUtil.getInt(temp[1]);
			}
			data.put(map+"-"+zone+"-"+type,this);
		} 
		catch (Exception e)
		{
			errorlogger.error("加载allthreestar奖励物品数据错误："+id,e);
			System.exit(0);
		}
	}

	@Override
	public void parse(String[] ss)
	{
	}

	@Override
	public void resetData()
	{
		data.clear();
	}

	public static AllThreeStarData getData(String key)
	{
		return data.get(key);
	}
}
