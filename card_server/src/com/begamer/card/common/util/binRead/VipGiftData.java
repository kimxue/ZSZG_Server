package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.begamer.card.common.util.StringUtil;
import com.begamer.card.log.ErrorLogger;

public class VipGiftData implements PropertyReader
{
	public int giftid;
	public String icon;
	public List<String> goods;
	
	private static HashMap<Integer, VipGiftData> data =new HashMap<Integer, VipGiftData>();
	private static final Logger errorlogger=ErrorLogger.logger;
	@Override
	public void addData()
	{
		data.put(giftid, this);
	}

	@Override
	public void parse(String[] ss)
	{
		int location =0;
		giftid =StringUtil.getInt(ss[location]);
		icon =StringUtil.getString(ss[location+1]);
		goods =new ArrayList<String>();
		for(int i=0;i<6;i++)
		{
			location =2+2*i;
			int type =StringUtil.getInt(ss[location]);
			if(type ==0)
			{
				continue;
			}
			String goodsId =StringUtil.getString(ss[location+1]);
			try
			{
				if(type<=5)
				{
					String [] temp =goodsId.split(",");
					int goodId =StringUtil.getInt(temp[0]);
					int num =StringUtil.getInt(temp[1]);
				}
				
			}
			catch (Exception e) 
			{
				errorlogger.error("加载vipgift表奖励物品出错:"+giftid,e);
				System.exit(0);
			}
			String good =type+"-"+goodsId;
			goods.add(good);
		}
		addData();
	}

	@Override
	public void resetData()
	{
		data.clear();
	}

	/**获取一个data**/
	public static VipGiftData getVipGiftData(int index)
	{
		return data.get(index);
	}
}
