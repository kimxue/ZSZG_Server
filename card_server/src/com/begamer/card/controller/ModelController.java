package com.begamer.card.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;

import com.begamer.card.common.util.StringUtil;
import com.begamer.card.model.dao.ModelDao;
import com.begamer.card.model.pojo.Model;

public class ModelController extends AbstractMultiActionController {
	
	@Autowired
	private ModelDao modelDao;
	
	/***************************************************************************
	 * 添加模板
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	public ModelAndView addModel(HttpServletRequest request, HttpServletResponse response)
	{
		int m_id = Integer.parseInt(request.getParameter("m_id"));
		String m_name = request.getParameter("m_name");
		int logo = Integer.parseInt(request.getParameter("logo"));
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String day = sdf.format(date);
		/** * */
		Model model = new Model();
		model.setM_id(m_id);
		model.setName(m_name);
		model.setLogo(logo);
		model.setCreateOn(day);
		modelDao.addModel(model);
		allModel(request, response);
		return new ModelAndView("/gm/allModel.vm");
	}
	
	/***************************************************************************
	 * 模板不启用
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	public ModelAndView delModelFalse(HttpServletRequest request, HttpServletResponse response)
	{
		int m_id = Integer.parseInt(request.getParameter("m_id"));
		int logo = 0;
		/**/
		Model model = new Model();
		model.setId(m_id);
		model.setLogo(logo);
		modelDao.delModelFalse(m_id, logo);
		allModel(request, response);
		return new ModelAndView("/gm/allModel.vm");
	}
	
	/***************************************************************************
	 * 删除模板
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	public ModelAndView delModelTrue(HttpServletRequest request, HttpServletResponse response)
	{
		Model model = new Model();
		model.setId(Integer.parseInt(request.getParameter("id")));
		model.setM_id(StringUtil.getInt(request.getParameter("m_id")));
		modelDao.delModelTrue(model);
		return allModel(request, response);
	}
	
	/*******
	 * 修改模板
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	public ModelAndView upModel(HttpServletRequest request, HttpServletResponse response)
	{
		int m_id = Integer.parseInt(request.getParameter("m_id"));
		int mid = Integer.parseInt(request.getParameter("mid"));
		String m_name = request.getParameter("m_name");
		String createOn = request.getParameter("createOn");
		int logo = Integer.parseInt(request.getParameter("logo"));
		/**/
		Model model = new Model();
		model.setId(m_id);
		model.setM_id(mid);
		model.setName(m_name);
		model.setCreateOn(createOn);
		model.setLogo(logo);
		modelDao.upModel(model);
		allModel(request, response);
		return new ModelAndView("/gm/allModel.vm");
	}
	
	/*******
	 * 全查询模板
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	public ModelAndView allModel(HttpServletRequest request, HttpServletResponse response)
	{
		boolean is = checkQx(getManager(request).getId());
		if (!is)
		{
			return new ModelAndView("gm/noqx.vm");
		}
		List<Model> list = modelDao.findAllModel();
		request.setAttribute("list", list);
		return new ModelAndView("/gm/allModel.vm");
	}
	
	/********
	 * 根据模板Id查询模板
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	public ModelAndView oneModelById(HttpServletRequest request, HttpServletResponse response)
	{
		int m_id = Integer.parseInt(request.getParameter("m_id"));
		Model model = modelDao.oneModel(m_id);
		request.setAttribute("model", model);
		return new ModelAndView("/gm/upModel.vm");
	}
	
	public ModelAndView addGo(HttpServletRequest request, HttpServletResponse response)
	{
		return new ModelAndView("gm/addModel.vm");
	}
}
