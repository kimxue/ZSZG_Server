package com.begamer.card.model.pojo;

import com.begamer.card.common.util.StringUtil;


public class PayRecord {

	private int id;
	private int playerId;
	private int cost;
	private int rechargeId;
	private String payDate;//格式yyyy-MM-dd HH:mm:ss
	
	public PayRecord()
	{}
	
	public static PayRecord createPayRecord(int playerId,int consumValue,int rechargeId)
	{
		PayRecord pr=new PayRecord();
		pr.playerId=playerId;
		pr.cost=consumValue;
		pr.rechargeId=rechargeId;
		pr.payDate=StringUtil.getDateTime(System.currentTimeMillis());
		return pr;
	}
	
	public int getId()
	{
		return id;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	public int getPlayerId()
	{
		return playerId;
	}
	public void setPlayerId(int playerId)
	{
		this.playerId = playerId;
	}
	public int getCost()
	{
		return cost;
	}
	public void setCost(int cost)
	{
		this.cost = cost;
	}
	public int getRechargeId()
	{
		return rechargeId;
	}
	public void setRechargeId(int rechargeId)
	{
		this.rechargeId = rechargeId;
	}
	public String getPayDate()
	{
		return payDate;
	}
	public void setPayDate(String payDate)
	{
		this.payDate = payDate;
	}
	
}
