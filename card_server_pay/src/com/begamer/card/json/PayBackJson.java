package com.begamer.card.json;

public class PayBackJson
{
	public String playerId;//
	public String rechargeId;//商品Id
	public String consumeValue;//消费金额,单位元
	
	public String getPlayerId()
	{
		return playerId;
	}
	public void setPlayerId(String playerId)
	{
		this.playerId = playerId;
	}
	public String getRechargeId()
	{
		return rechargeId;
	}
	public void setRechargeId(String rechargeId)
	{
		this.rechargeId = rechargeId;
	}
	public String getConsumeValue()
	{
		return consumeValue;
	}
	public void setConsumeValue(String consumeValue)
	{
		this.consumeValue = consumeValue;
	}
}
